extends MeshInstance3D


@export var debug_symbol_indicator_resource: Resource

@export var symbols: PackedStringArray

signal reel_landed_on(result: ReelResult)

const SPIN_AXIS: Vector3 = Vector3.RIGHT
const SPIN_SPEED: int = 8
@onready var number_of_symbols: int = len(symbols)
@onready var symbol_angle: float = 360.0 / float(number_of_symbols)

@onready var timer = get_node("Timer")

var reel_number: int

var spinning: bool = false
var current_angle: float = 0.0

func _ready():
	var i: int = 0
	for symbol in symbols:
		var debug_symbol_indicator = debug_symbol_indicator_resource.instantiate()
		add_child(debug_symbol_indicator)
		debug_symbol_indicator.get_node("Label3D").text = symbol
		debug_symbol_indicator.global_transform = debug_symbol_indicator.global_transform.rotated(SPIN_AXIS, -deg_to_rad(symbol_angle * i))
		i += 1

func do_a_spin(time: float):
	print(time)
	timer.wait_time = time
	_start_spinning()
	timer.start()

func _process(_delta):
	if spinning:
		_spin()

func _start_spinning():
	print(current_angle)
	spinning = true

func _stop_spinning():
	if not spinning:
		return

	var angle_delta: float = fmod(current_angle, symbol_angle)
	current_angle -= angle_delta
	transform = transform.rotated(SPIN_AXIS, deg_to_rad(-angle_delta))
	spinning = false
	
	var reel_position = _angle_to_reel_position(current_angle)
	var reel_result = ReelResult.new()
	reel_result.reel_number = reel_number
	reel_result.position = reel_position
	reel_result.symbol = symbols[int(reel_position)]
	reel_result.symbol_before = symbols[(int(reel_position) - 1) % number_of_symbols]
	reel_result.symbol_after = symbols[(int(reel_position) + 1) % number_of_symbols]
	
	reel_landed_on.emit(reel_result)

func _angle_to_reel_position(angle):
	return fmod(angle / symbol_angle, float(number_of_symbols))

func _spin():
	current_angle = fmod(current_angle, 360.0)
	current_angle += SPIN_SPEED
	transform = transform.rotated(SPIN_AXIS, deg_to_rad(SPIN_SPEED))


func _on_timer_timeout():
	print("timeout")
	_stop_spinning()

func _on_reel_landed_on(reel_result):
	print("Reel position " + str(reel_result.position))
	print("Reel before: " + str(reel_result.symbol_before))
	print("Reel symbol: " + str(reel_result.symbol))
	print("Reel after: " + str(reel_result.symbol_after))
