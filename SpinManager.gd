extends Node


const REEL_MINIMUM_SPIN_TIME: float = 3.0
const REEL_SEPARATION_TIME: float = 1.5

@export var reel_1_path: NodePath
@export var reel_2_path: NodePath
@export var reel_3_path: NodePath

@onready var reel1 = get_node(reel_1_path)
@onready var reel2 = get_node(reel_2_path)
@onready var reel3 = get_node(reel_3_path)

var number_of_reels: int = 3

var reels_finished: int = 0

# TODO: WE'RE ASSUMING THAT THIS LIST IS ORDERED BECAUSE WE EXPECT THE REELS TO STOP IN A SPECIFIC ORDER VISUALLY.
var reel_results: Array = []

var state: String = "idle"

var credits: int = 100

var modes = ["1", "3", "5"]

var mode = "1"

@onready var credits_label = get_node("RichTextLabel")
@onready var modes_label = get_node("RichTextLabel2")

@onready var line1 = get_node("Lines/1")
@onready var line2 = get_node("Lines/2")
@onready var line3 = get_node("Lines/3")
@onready var line4 = get_node("Lines/4")
@onready var line5 = get_node("Lines/5")

func _ready():
	_cycle_modes()

func _process(_delta):
	credits_label.text = str(credits)
	modes_label.text = str(mode)

func _input(event):
	if not state == "idle":
		return

	if event.is_action_pressed("ui_accept"):
		_cycle_modes()

	if event.is_action_pressed("ui_up"):
		_spin_the_reels()

func _cycle_modes():
	var next_mode = modes.pop_front()
	mode = next_mode
	modes.append(next_mode)

	if mode == "1":
		line1.visible = true
		line2.visible = false
		line3.visible = false
		line4.visible = false
		line5.visible = false

	if mode == "3":
		line1.visible = true
		line2.visible = true
		line3.visible = true
		line4.visible = false
		line5.visible = false

	if mode == "5":
		line1.visible = true
		line2.visible = true
		line3.visible = true
		line4.visible = true
		line5.visible = true

func _spin_the_reels():
	state = "spinning"

	if mode == "1":
		credits -= 1
	if mode == "3":
		credits -= 3
	if mode == "5":
		credits -= 5

	reel_results = []
	reels_finished = 0
	
	var spin_times = []
	spin_times.append(REEL_MINIMUM_SPIN_TIME + (randf() / 10.0))
	spin_times.append(REEL_MINIMUM_SPIN_TIME + REEL_SEPARATION_TIME + (randf() / 10.0))
	spin_times.append(REEL_MINIMUM_SPIN_TIME + REEL_SEPARATION_TIME  + REEL_SEPARATION_TIME + (randf() / 10.0))

	reel1.do_a_spin(spin_times.pop_front())
	reel2.do_a_spin(spin_times.pop_front())
	reel3.do_a_spin(spin_times.pop_front())

func _on_reel_reel_landed_on(result):
	reel_results.append(result)
	reels_finished += 1

	if reels_finished == number_of_reels:
		_do_payout()

func _on_reel_2_reel_landed_on(result):
	reel_results.append(result)
	reels_finished += 1

	if reels_finished == number_of_reels:
		_do_payout()

func _on_reel_3_reel_landed_on(result):
	reel_results.append(result)
	reels_finished += 1

	if reels_finished == number_of_reels:
		_do_payout()

func _do_payout():
	_match_reels(reel_results)
	print("payout")
	state = "idle"

func _is_win(one, two, three) -> int:
	if (one == two) and (two == three):
		return 3

	return 0

func _match_reels(reels):
	var wins = []

	if mode == "1":
		wins = _match_mode_1(reels, wins)

	if mode == "3":
		wins = _match_mode_3(reels, wins)

	if mode == "5":
		wins = _match_mode_5(reels, wins)

	for win in wins:
		credits += win

	print(wins)

func _match_mode_1(reels, wins):
	# Middle.
	wins.append(_is_win(reels[0].symbol, reels[1].symbol, reels[2].symbol))

	return wins

func _match_mode_3(reels, wins):
	wins = _match_mode_1(reels, wins)

	# TLBR
	wins.append(_is_win(reels[0].symbol_after, reels[1].symbol, reels[2].symbol_before))

	# BLTR
	wins.append(_is_win(reels[0].symbol_before, reels[1].symbol, reels[2].symbol_after))

	return wins

func _match_mode_5(reels, wins):
	wins = _match_mode_3(reels, wins)

	# Top.
	wins.append(_is_win(reels[0].symbol_after, reels[1].symbol_after, reels[2].symbol_after))

	# Bottom
	wins.append(_is_win(reels[0].symbol_before, reels[1].symbol_before, reels[2].symbol_before))

	return wins


